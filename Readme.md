Simple Foobar2k
===============

Основные принципы:

 - Нативный интерфейс;
 - Минимальная нагрузка на систему;
 - Максимальное быстродействие;
 - **TODO:** Обновления в фоновом режиме.

Особенности:

 - Columns UI;
 - `Ctrl-P` для открытия окна настроек;
 - `Ctrl-Q` для выхода;
 - Вместе с компонентом конвертер поставляются свободные энкодеры;
 - Рекомендуется держать библиотеку в чистоте (Без сборников "лучших" хитов).



Сборка
------

 1. Скачиваем [Foobar2k](http://www.foobar2000.org/);
 2. Устанавливаем как Portable в директорию `app` сборщика;
 3. Скачиваем и устанавливаем [Inno Setup](http://www.jrsoftware.org/isdl.php)
 4. Запускаем `Build.bat`.

На все файлы для сборки лицензия MIT, Creative Commons или своих авторов.



THANKS:
-------

DarKobra:

 * `Foobar2k.ico` - http://darkobra.deviantart.com/art/Tango-Foobar2000-Icon-81632375
 * `Foobar2k_installer.ico` (with my changes) - http://darkobra.deviantart.com/art/Tango-Pokeball-Icon-112774756

Angus Johnson:

 * `tools\ResHacker.exe` - http://www.angusj.com/resourcehacker/

**and foobar2000 author with contributors!**