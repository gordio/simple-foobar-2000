; -- Foobar 2000.iss --
; Modification of Foobar 2000 with unofficial components

#define prog_name "Foobar 2000"

[Setup]
AppName={#prog_name}
AppId={#prog_name}
AppVersion=1.2.9.0-r1
AppComments=Audio player based on Foobar 2000
AppPublisher=Gordio
AppSupportURL=http://code.gordio.pp.ua/simple-foobar-2000

DefaultDirName={pf32}\{#prog_name}
DefaultGroupName={#prog_name}
UninstallDisplayIcon={app}\foobar2000.exe
Compression=lzma2/ultra
InternalCompressLevel=max
SolidCompression=yes
SetupIconFile=Foobar2k_installer.ico
AllowNoIcons=yes
ChangesAssociations=yes
OutputDir=Build
OutputBaseFilename=Setup Foobar 2000

[Types]
Name: "full"; Description: "{cm:FullInstall}"
Name: "compact"; Description: "{cm:CompactInstall}"
Name: "custom"; Description: "{cm:CustomInstall}"; Flags: iscustom

[Components]
Name: "program"; Description: "{#prog_name} files"; Types: full compact custom; Flags: fixed
Name: "foo_converter"; Description: "Convert files (with free encoders)"; Types: full
Name: "foo_cdda"; Description: "Audio CD Support"; Types: full
Name: "foo_dsp_std"; Description: "Standart DSP"; Types: full
Name: "foo_dsp_eq"; Description: "Equalizer DSP"; Types: full
Name: "foo_rgscan"; Description: "ReplayGain Scanner"; Types: full compact custom
Name: "foo_fileops"; Description: "File Operations"; Types: full
Name: "foo_freedb2"; Description: "FreeDB Online Tagger"; Types: full
Name: "help_license"; Description: "Help and Licence Files"; Types: full

[Dirs]
Name: "{app}\components"; Tasks: normal_install
Name: "{app}\components"; Permissions: users-modify; Tasks: portable_install
Name: "{app}\playlists"; Permissions: users-modify; Tasks: portable_install
Name: "{app}\configuration"; Permissions: users-modify; Tasks: portable_install

[Files]
Source: "app\foobar2000.exe"; DestDir: "{app}"; Components: program; Flags: ignoreversion
Source: "app\Foobar2000 Shell Associations Updater.exe"; DestDir: "{app}"; Components: program; Flags: ignoreversion
Source: "app\shared.dll"; DestDir: "{app}"; Components: program; Flags: ignoreversion
Source: "app\ShellExt32.dll"; DestDir: "{app}"; Components: program; Flags: ignoreversion
Source: "app\ShellExt64.dll"; DestDir: "{app}"; Components: program; Flags: ignoreversion
Source: "app\avcodec-fb2k-54.dll"; DestDir: "{app}"; Components: program; Flags: ignoreversion
Source: "app\avutil-fb2k-52.dll"; DestDir: "{app}"; Components: program; Flags: ignoreversion
Source: "app\libpng13.dll"; DestDir: "{app}"; Components: program; Flags: ignoreversion
Source: "app\zlib1.dll"; DestDir: "{app}"; Components: program; Flags: ignoreversion
Source: "Readme.txt"; DestDir: "{app}"; Components: program; Flags: isreadme

; Normal installation
Source: "user_profiles_enabled"; DestDir: "{app}"; Components: program; Flags: ignoreversion; Tasks: normal_install
Source: "Core.cfg"; DestDir: "{userappdata}\foobar2000\configuration"; Components: program; Flags: confirmoverwrite; Tasks: normal_install
Source: "foo_albumlist.dll.cfg"; DestDir: "{userappdata}\foobar2000\configuration"; Components: program; Flags: confirmoverwrite; Tasks: normal_install
Source: "foo_ui_columns.dll.cfg"; DestDir: "{userappdata}\foobar2000\configuration"; Components: program; Flags: confirmoverwrite; Tasks: normal_install
; Portable
Source: "portable_mode_enabled"; DestDir: "{app}"; Components: program; Flags: ignoreversion; Tasks: portable_install
Source: "Core.cfg"; DestDir: "{app}\configuration"; Components: program; Permissions: users-modify; Flags: confirmoverwrite; Tasks: portable_install
Source: "foo_albumlist.dll.cfg"; DestDir: "{app}\configuration"; Components: program; Permissions: users-modify; Flags: confirmoverwrite;Tasks: portable_install
Source: "foo_ui_columns.dll.cfg"; DestDir: "{app}\configuration"; Components: program; Permissions: users-modify; Flags: confirmoverwrite;Tasks: portable_install

; Components
Source: "app\components\foo_ui_std.dll"; DestDir: "{app}\components"; Components: program; Flags: ignoreversion
Source: "app\components\foo_ui_columns.dll"; DestDir: "{app}\components"; Components: program; Flags: ignoreversion
Source: "app\components\foo_albumlist.dll"; DestDir: "{app}\components"; Components: program; Flags: ignoreversion
Source: "app\components\foo_input_std.dll"; DestDir: "{app}\components"; Components: program; Flags: ignoreversion
; components\foo_converter
Source: "app\components\foo_converter.dll"; DestDir: "{app}\components"; Components: foo_converter; Flags: ignoreversion
Source: "app\encoders\COPYING.GPL"; DestDir: "{app}\encoders"; Components: help_license; Flags: ignoreversion
Source: "app\encoders\COPYING.txt"; DestDir: "{app}\encoders"; Components: help_license; Flags: ignoreversion
Source: "app\encoders\license.txt"; DestDir: "{app}\encoders"; Components: help_license; Flags: ignoreversion
Source: "app\encoders\flac.exe"; DestDir: "{app}\encoders"; Components: foo_converter; Flags: ignoreversion
Source: "app\encoders\mpcenc.exe"; DestDir: "{app}\encoders"; Components: foo_converter; Flags: ignoreversion
Source: "app\encoders\oggenc2.exe"; DestDir: "{app}\encoders"; Components: foo_converter; Flags: ignoreversion
Source: "app\encoders\opusenc.exe"; DestDir: "{app}\encoders"; Components: foo_converter; Flags: ignoreversion
Source: "app\encoders\wavpack.exe"; DestDir: "{app}\encoders"; Components: foo_converter; Flags: ignoreversion
; components\foo_cdda
Source: "app\components\foo_cdda.dll"; DestDir: "{app}\components"; Components: foo_cdda; Flags: ignoreversion
; components\foo_dsp_std
Source: "app\components\foo_dsp_std.dll"; DestDir: "{app}\components"; Components: foo_dsp_std; Flags: ignoreversion
; components\foo_dsp_eq
Source: "app\components\foo_dsp_eq.dll"; DestDir: "{app}\components"; Components: foo_dsp_eq; Flags: ignoreversion
; components\foo_rgscan
Source: "app\components\foo_rgscan.dll"; DestDir: "{app}\components"; Components: foo_rgscan; Flags: ignoreversion
; components\foo_fileops
Source: "app\components\foo_fileops.dll"; DestDir: "{app}\components"; Components: foo_fileops; Flags: ignoreversion
; components\foo_freedb2
Source: "app\components\foo_freedb2.dll"; DestDir: "{app}\components"; Components: foo_freedb2; Flags: ignoreversion
; Help
Source: "app\help\Query Syntax Help.html"; DestDir: "{app}\Help"; Components: help_license; Flags: ignoreversion
Source: "app\help\titleformat_help.html"; DestDir: "{app}\Help"; Components: help_license; Flags: ignoreversion
Source: "app\help\titleformat_help.css"; DestDir: "{app}\Help"; Components: help_license; Flags: ignoreversion

[Icons]
Name: "{group}\{#prog_name}"; Filename: "{app}\foobar2000.exe"
Name: "{group}\{#prog_name} Preferences"; Filename: "{app}\foobar2000.exe"; Parameters: "/config"
Name: "{group}\{#prog_name} Components Directory"; Filename: "{app}\components\"
Name: "{group}\{#prog_name} Help"; Filename: "{app}\help\"; Components: help_license
Name: "{group}\Website"; Filename: "http://code.gordio.pp.ua/simple-foobar-2000"
Name: "{group}\Uninstall {#prog_name}"; Filename: "{uninstallexe}"
Name: "{commondesktop}\{#prog_name}"; Filename: "{app}\foobar2000.exe"; WorkingDir: "{app}"; Tasks: desktopicon

[Run]
Filename: "{app}\foobar2000.exe"; WorkingDir: "{app}"; Description: "{cm:LaunchProgram,{#prog_name}}"; Flags: postinstall skipifsilent nowait
;Filename: "{app}\foobar2000 Shell Associations Updater.exe"; WorkingDir: "{app}"; Description: "{cm:LaunchProgram,{#prog_name}}"; Flags: postinstall skipifsilent nowait; Tasks: file_assoc

[Tasks]
Name: "desktopicon"; Description: "{cm:CreateDesktopIcon}";  
;Name: "file_assoc"; Description: "{cm:FileAssociate, {#prog_name}}"
;Name: "folder_assoc"; Description: "{cm:FolderAssociate, {#prog_name}}"
Name: "normal_install"; Description: "{cm:NormalInstallation}"; GroupDescription: "{cm:InstallMode}";  Flags: exclusive
Name: "portable_install"; Description: "{cm:PortableInstallation}"; GroupDescription: "{cm:InstallMode}";  Flags: exclusive unchecked

[Registry]
;[HKEY_CLASSES_ROOT\Directory\shell\foobar2000.play]
;@="{cm:FolderAssociateName, {#prog_name}}"
;"LegacyDisable"=""
;[HKEY_CLASSES_ROOT\Directory\shell\foobar2000.play\Command]
;@="\"{app}\\foobar2000.exe\" \\add \"%1\""

;Root: HKCR; Subkey: ".m3u"; ValueType: string; ValueName: ""; ValueData: "{app}\foobar2000.exe"; Flags: uninsdeletevalue; Tasks: file_assoc 
;Root: HKCR; Subkey: "MyProgramFile"; ValueType: string; ValueName: ""; ValueData: "My Program File"; Flags: uninsdeletekey; Tasks: file_assoc

[Languages]
Name: en; MessagesFile: "compiler:Default.isl"
Name: ru; MessagesFile: "compiler:Languages\Russian.isl"

[Messages]
en.BeveledLabel=English
ru.BeveledLabel=�������

[CustomMessages]
en.FullInstall=Full installation
en.CompactInstall=Compact installation
en.CustomInstall=Custom installation
en.InstallMode=Installation Mode:
en.NormalInstallation=Normal
en.PortableInstallation=Portable
en.FileAssociate=Associate %1 with supported audio files
en.FolderAssociate=Context menu for folders "Add to %1"
en.FolderAssociateName=Add to %1
ru.FullInstall=������ ���������
ru.CompactInstall=����������� ���������
ru.CustomInstall=�������������� ���������
ru.InstallMode=����� ���������:
ru.NormalInstallation=������� ���������
ru.PortableInstallation=����������� ������
ru.FileAssociate=��������� %1 ��� ��������� �� ���������� ��� ����� ������
ru.FolderAssociate=�������� ����������� ���� � ����������� "�������� � %1"
ru.FolderAssociateName=�������� � %1